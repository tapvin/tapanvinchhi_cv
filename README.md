# README #

This README documents what this repo is for and how to create a CV in pdf format using AsciiDoctor.

### What is this repository for? ###

* This repository hold my Curriculum Vitae created in AsciiDoc
* 1.0

### How do I get set up? ###
Pre-requisite you need Windows 10 installed PC/laptop

* Install Ruby
* Install any text editor of your choice. I use Visual Code for editing my CV in asciidoc format.
* Using ```gem install asciidoctor``` install AsciiDoctor
* Using ```gem install asciidoctor-pdf``` install AsciiDoctor pdf
* Currently I have created ```custom-theme.yml``` to override heading font and color over default values.
* Execute ```asciidoctor-pdf .\TapanVinchhi.adoc -a pdf-style=custom-theme.yml``` command to generate CV in pdf

### Important links ###

* https://asciidoctor.org/
* https://asciidoctor.org/docs/asciidoctor-pdf/
* https://asciidoctor.org/docs/asciidoctor-pdf/#themes

